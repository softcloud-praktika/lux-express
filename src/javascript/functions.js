$(document).ready(function () {

  var Summary = {
    total: 0,
    $el: {
      price: $(".summary-item__price"),
      finalPrice: $('.summary__container__price'),
      from: $('.depFrom'),
    },
    changePrice: function (price) {
      this.total += parseFloat(price);
      this.$el.price.html(this.total);
      this.$el.finalPrice.html(this.total);

    },
    resetPrice: function (DepOrArr) {
      this.total = 0;
      this.$el.price.html(this.total);
      this.$el.finalPrice.html(this.total);
    },


  }


  var TripTemplate = function ($element) {
    var _this = this;
    function init() {
      _this.setPrice(20);

    }
    var $el = {
      price: $element.find('.inner__traveller-info__price'),
      departureTime: $element.find('.departure'),
      arrivalTime: $element.find('.arrival'),
      duration: $element.find('.duration'),
      travellers: $element.find('.traveller-info'),
      bus: $element.find('.bus-company'),
    }
    this.setPrice = function (price) {
      $($el.price).html(price);
    }
    this.setDeparture = function (time) {
      $($el.departureTime).html(time);
    }
    this.setArrival = function (time) {
      $($el.arrivalTime).html(time);
    }
    this.setCompany = function (company) {
      $($el.bus).html(company);
    }
    this.setDuration = function (time) {

      $($el.duration).html(time);
    }
    this.setTravellers = function (people) {
      $($el.travellers).html(people);
    }
    this.getPrice = function () {

      console.log($($el.price).html());

    }
    init();

  }

  //Form submit

  var $form = $('.menu-form');
  $form.find('.find').on('click', findTrip);

  var findTrips = $form.serializeArray();


  function findTrip() {
    findTrips = $form.serializeArray(); //paneb otsingu parameetrid arraysse

    var departureStation = findTrips[0].value;
    var arrivalStation = findTrips[1].value;
    var departureDate = convertDate(findTrips[2].value);
    var arrivalDate = convertDate(findTrips[3].value);
    var passengers = findTrips[findTrips.length-1].value;
    




    //muudab reisi algus- ja lõpppunkti, reisijate arvu
    $('.departures').find('.from').text(departureStation);
    $('.departures').find('.to').text(arrivalStation);
    $('.arrivals').find('.from').text(arrivalStation);
    $('.arrivals').find('.to').text(departureStation);
    $('.summary-item-container-left').find('.depFrom').text(departureStation);
    $('.summary-item-container-left').find('.arrTo').text(arrivalStation);
    $('.passengersText').find('.passengers').text(passengers);
    changeText();

    if(findTrips[4].name=='havePet'){
      $('.pets').text('1 lemmikloom');

    }else{
      $('.pets').text('');
    }

    //muudab kuupäevad

    $('.summary-item-container-left').find('.depDate').text(departureDate);
    $('.summary-item-container-left').find('.arrDate').text(arrivalDate);

    changeDates(findTrips[2].value, 'departures');
    changeDates(findTrips[3].value, 'arrivals');

    //muudab slideri current elemendi ja kuvab reisid

    var $carouselElements = $('.carousel-element');
    for (var i = 0; i < $carouselElements.length; i++) {
      var $elemDate = $($carouselElements[i]).data('date');
      var $elemStation = $($carouselElements[i]).data('type');
      if ($elemDate == findTrips[2].value && $elemStation == 'departures') {
        $($carouselElements[i]).click();
      } else if ($elemDate == findTrips[3].value && $elemStation == 'arrivals') {
        $($carouselElements[i]).click();
      }
    }

    if (findTrips[3].value == "") { //kui tagasisõidu kuupäev pole määratud, peida arrival reisid
      $('.arrivals').find('.trip').hide();

    }










  }

  function convertDate(date) { //kujult 2020-12-19 -> 19.detsember

    var day = date.slice(8, 10);
    var newMonth;
    var getMonth = date.slice(5, 7);

    var months = new Array(12);
    months[0] = "";
    months[1] = "jaanuar";
    months[2] = "veebruar";
    months[3] = "märts";
    months[4] = "aprill";
    months[5] = "mai";
    months[6] = "juuni";
    months[7] = "juuli";
    months[8] = "august";
    months[9] = "september";
    months[10] = "oktoober";
    months[11] = "november";
    months[12] = "detsember";

    for (var i = 0; i < months.length; i++) {
      if (i == getMonth) {
        newMonth = months[i];
      }
    }
    var newDate = day + '.' + newMonth;
    return newDate;

  }






  // uuendab kuupäevad sliderilt uut kuupäeva valides
  function changeDates(newDate, depOrArr) {

    var depDate = convertDate(newDate);
    var $depOrArrElem = document.getElementsByClassName(depOrArr);
    $($depOrArrElem).find('.date').text(depDate);

    if (depOrArr == 'departures') {
      $('.depDate').text(depDate);
    } else {
      $('.arrDate').text(depDate);
    }

  }



  //4. Valides slideril uue kuupäeva, näitab selle kuupäeva vastavaid reise (departure või arrival reisid), uuendab kokkuvõtte summat ja kuupäevi


  var $tripsOnDate = $('.choose-date__container');
  $tripsOnDate.find('.carousel-element').on('click', function () {
    var type = $(this).data('type');
    var $selectedBtn = $('.' + type).find('.selected-btn');
    if ($selectedBtn) {
      $selectedBtn.trigger('click');
    }
    generateTrips(this)
  });










  function filterTrips(date, departureFrom, arriveTo) {
    var newArr = [];
    for (var i = 0; i < trips.length; i++) {
      if (trips[i].departureDate === date && trips[i].from === departureFrom && trips[i].to === arriveTo) {
        newArr.push(trips[i]);
      }
    }
    return newArr;
  }

  function resetButton(btnToChange) {
    btnToChange.text("VALI");
    btnToChange.removeClass("selected-btn");
    btnToChange.addClass("select-btn");
  }

  function generateTrips(_this) {
    var i;
    var departureDate = $(_this).data('date');
    var depOrArrival = document.getElementsByClassName($(_this).data('type')); //valib, kas arrival või departure reisid
    var from = $(depOrArrival).find('.from').html();
    var to = $(depOrArrival).find('.to').html();
    var tripItems = filterTrips(departureDate, from, to); //filtreerib vastavalt väljumiskuupäevale ja -kohale
    var $trip = $('.trip')[0];  //salvestan tühja elemendi, mida hiljem kloonida
    changeDates(departureDate, $(_this).data('type'));
    $(depOrArrival).find('.trip').detach(); //eemaldab eelmise valiku tripid

    for (i = 0; i < tripItems.length; i++) {
      var currentTrip = tripItems[i];
      var $tripEl = $($trip).clone(); //kloonib esimese elemendi

      $(depOrArrival).append($tripEl); //lisab selle departure div-i
      var elem = new TripTemplate($tripEl);
      $tripEl.on('click', showTrips);
      //kui valid uue kuupäeva, läheb reisi Muuda nupp tagasi VALI nupuks
      var btnsToChange = $(depOrArrival).find('.selected-btn');
      resetButton(btnsToChange);

      elem.setArrival(currentTrip.arrival);
      elem.setDeparture(currentTrip.departure);
      elem.setPrice(currentTrip.price);
      elem.setCompany(currentTrip.bus);
      elem.setDuration(currentTrip.duration);

    }


  }





  //2. Valides reisi (vajutades nupul "VALI"), peidab/näitab teised sama kuupäevaga reisid, 
  //muudab "VALI" nupu "Muuda"nupuks (või vastupidi) ja arvutab kokkuvõttesse hinna


  function showTrips(event) {

    var $this = $(this),
      $depOrArr = $this.parent(),
      $tripsToHide = $depOrArr.find(".trip"),
      $btnChange = $this.find('button');
    //peidab mitte-aktiivsed reisid ja muudab nupu
    $tripsToHide.toggle();
    $this.show();
    changeButton($btnChange, $this);

  }

  function changeButton(btnToChange, $container) {
    var price = $container.find('.inner__traveller-info__price').html();
    var $btnChange = btnToChange;

    if ($btnChange.text() == "VALI") {
      $btnChange.text("Muuda");
      $btnChange.removeClass("select-btn");
      $btnChange.addClass("selected-btn");
      Summary.changePrice(price);
    } else {
      $btnChange.text("VALI");
      $btnChange.removeClass("selected-btn");
      $btnChange.addClass("select-btn");
      Summary.changePrice(-price);
    }

  }
  var $departures = $('.departures');
  $departures.find('.trip').on('click', showTrips);

  var $arrivals = $('.arrivals');
  $arrivals.find('.trip').on('click', showTrips);




  //1. Lehe laadimisel kuvab aktiivse kuupäevaga reisid
  var $currentElement = $('.carousel-element');
  $currentElement[0].click();
  $currentElement[7].click();




  //Kuupäeva valiku slider
  var slider = $('.choose-date__container').slick({
    centerMode: true,
    arrows: true,
    centerPadding: '0px',
    slidesToShow: 5,
    focusOnSelect: true,
    prevArrow: '<button type="button" class="material-icons left">arrow_left</button>',
    nextArrow: '<button type="button" class="material-icons right">arrow_right</button>',
    responsive: [
      {
        breakpoint: 768,
        settings: {
          centerMode: true,
          focusOnSelect: true,
          arrows: true,
          centerPadding: '40px',
          slidesToShow: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          centerMode: true,
          focusOnSelect: true,
          arrows: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      }
    ]

  });

  var $arrow = $tripsOnDate.find('.slick-arrow');

  $arrow.on('click', function () {
    var $current = $tripsOnDate.find('.slick-current');
    var $this = $(this);
    var type = $this.parent().data('type');
    var $selectedBtn = $('.' + type).find('.selected-btn');
    if ($selectedBtn) {
      $selectedBtn.trigger('click');
    }
    if (type == 'departures') {
      generateTrips($current[0]);
    } else {
      generateTrips($current[1]);
    }


  });



  //Otsi reisi menüü funktsioonid

  //1. peidab ja avab menüü

  var $menu = $('.menu');
  $menu.find('.close').on('click', showMenu);
  $('.menu-icon').on('click', showMenu);
  function showMenu() {

    if (!($menu).hasClass('menu__show')) { //kui menüü on nähtav
      $menu.addClass('menu__show');
    } else {
      $menu.removeClass('menu__show');

    }
  }

  //reisijate arvu suurendamine/vähendamine
  var $passengers = $('.passengers');
  $passengers.find('.add').on('click', addPassenger);
  $passengers.find('.subtract').on('click', subPassenger);
  var $passengerNumber = $('.passengerNr');

  function addPassenger() {
    var nr = $passengerNumber.val();
    nr++;
    $passengerNumber.val(nr);
    changeText();
  }

  function subPassenger() {
    var nr = $passengerNumber.val();
    nr--;
    if (nr < 0) {
      $passengerNumber.val(0);
    } else {
      $passengerNumber.val(nr);
    }

    changeText();

  }

  function changeText() {
    if ($passengerNumber.val() == 1) {
      $('.passengerText').text('reisija');
      $('.ptext').text('reisija');

    } else {
      $('.passengerText').text('reisijat');
      $('.ptext').text('reisijat');
    }

  }


















});


